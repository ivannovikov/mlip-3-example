This repository contains an example of using active learning on atomic neighborhoods (with the MLIP-3 package) during molecular dynamics in LAMMPS.
In the example we grow a copper crystal on the <111> surface by depositing copper atoms on the copper layers (substrate).
The repository contains:

- cu-structure-growing-input: is the folder that contains the input files and the scripts.
- cu-structure-growing-output: is the folder that contains all the input and output files.

Each folder contains README.md with the description of the files and the usage of them.

