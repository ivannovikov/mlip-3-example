#!/bin/bash
src=$(pwd)
cpus_on_node=$SLURM_CPUS_ON_NODE
n_cores=$cpus_on_node
cd $src
echo "pre-train is starting"
mpirun -n $n_cores ./mlp train curr.almtp train.cfg --iteration_limit=500 --al_mode=nbh --energy_weight=1 --force_weight=0.01 --stress_weight=0 --save_to=curr.almtp --init_random=true
echo "pre-train completed"
echo "1" >> is_training_finished.txt

