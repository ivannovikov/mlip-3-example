#!/bin/bash

source ~/.bashrc
n_cores=$1
rm -f preselected.cfg.*
rm -f preselected.cfg
rm -f selected.cfg
rm -f nbh.cfg
rm -f *.txt
while [ 1 -gt 0 ]
do
cp curr.almtp pot_save/
touch preselected.cfg
is_lammps_finished=0
sbatch -J lammps -o lammps.out -e lammps.err  -N 1 -n 16 -p high,MMM ./run_lammps.sh
while [ $is_lammps_finished -ne 1 ]
do
    sleep 5
    if [ -f is_lammps_finished.txt ]; then
        is_lammps_finished=1
        echo $is_lammps_finished
    else
        is_lammps_finished=0
        echo $is_lammps_finished
    fi
done
sleep 5
cat preselected.cfg.* >> preselected.cfg
rm -f preselected.cfg.*
n_preselected=$(grep "BEGIN" preselected.cfg | wc -l)
is_selection_finished=0
if [ $n_preselected -gt 0 ]; then
    echo "selection"
    sbatch -J selection -o selection.out -e selection.err  -N 1 -n 1 -p bigmem ./select.sh
    while [ $is_selection_finished -ne 1 ]
    do
        sleep 5
        if [ -f is_selection_finished.txt ]; then
            is_selection_finished=1
            echo $is_selection_finished
        else
            is_selection_finished=0
            echo $is_selection_finished
        fi
    done
    sleep 5
    if test -f selected.cfg; then
        echo " selected.cfg exists"
    else
        echo "selected.cfg not exist"
        exit
    fi
    rm -f preselected.cfg
    ./mlp cut_extrapolative_nbh selected.cfg nbh.cfg --cutoff=8 
    ./mlp convert nbh.cfg POSCAR --output_format=poscar 
    rm -f selected.cfg
    n_poscar=$(grep "BEGIN" nbh.cfg | wc -l)
    n_poscar_prev=$((${n_poscar}-1))
    if [ $n_poscar -eq 1 ]; then
        mv POSCAR POSCAR0
    fi
    for p in $(seq 0 ${n_poscar_prev})
    do
        python fill_with_vacuum.py $p
    done
    for ((i=0; i<$n_poscar; i++))
    do
        cp POSCAR_output"$i" VASP/
        mkdir -p VASP/"$i"
        if [ $n_poscar -eq 1 ]; then
            mv VASP/POSCAR_output"$i" VASP/0/POSCAR
        elif [ $n_poscar -gt 1 ]; then
            mv VASP/POSCAR_output"$i" VASP/"$i"/POSCAR
        fi
        cp VASP/POTCAR VASP/"$i"/
        cp VASP/INCAR VASP/"$i"/
        cp VASP/KPOINTS VASP/"$i"/
        cp VASP/run_vasp.sh VASP/"$i"/
        cp VASP/sub.sh VASP/"$i"/
    done
    for ((i=0; i<$n_poscar; i++))
    do
        cd VASP/"$i"
        sbatch ./run_vasp.sh
	cd ../../
    done
    is_vasp_finished=0
    while [ $is_vasp_finished -ne 1 ]
    do
        sleep 5
        n_vasp_finished=$(grep -rH "1" VASP/*/is_vasp_finished.txt | wc -l)
        if [ $n_poscar -eq $n_vasp_finished ]; then
            is_vasp_finished=1
            echo $is_vasp_finished
        else
            is_vasp_finished=0
            echo $is_vasp_finished
        fi
    done
    sleep 5
    for ((i=0; i<$n_poscar; i++))
    do
        ./mlp convert VASP/"$i"/OUTCAR VASP/"$i"/calculated.cfg --input_format=outcar #--elements_order=29 --absolute_elements
	cat VASP/"$i"/calculated.cfg >> train.cfg
        rm -r VASP/"$i"/
    done
    is_training_finished=0
    sbatch -J train -o train.out -e train.err  -N 1 --exclusive -p high,MMM ./train.sh --time=3:00:00
    while [ $is_training_finished -ne 1 ]
    do
        sleep 5
        if [ -f is_training_finished.txt ]; then
            is_training_finished=1
            echo $is_training_finished
        else
            is_training_finished=0
            echo $is_training_finished
        fi
    done
    sleep 5
    rm -f nbh.cfg
    rm *.txt
    rm POSCAR*
    rm -f nbh_36.cfg    
elif [ $n_preselected -eq 0 ]; then
    exit
fi


done
