#!/bin/sh

echo $(pwd)

sbatch -p high -N 1 -n 36 -D $(pwd) --exclude=node-mmm01,node-mmm02,node-mmm03,node-mmm04,node-mmm05,node-mmm06,node-mmm07,node-mmm08,node-mmm09,node-mmm10,node-mmm11,node-mmm12,node-mmm13,node-mmm14 run_vasp.sh
