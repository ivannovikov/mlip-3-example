This folder contains the files needed for the active learning of the potential during molecular dynamics (MD), 
the folder VASP/ for DFT calculations, and the folder pot_save/ where the MD trajectory, training sample 
and potential will be saved.

Input files and scripts:
- train_one.cfg contains one configuration for potential pretraining
- train.sh: shell script for training an MTP
- init.almtp is the file with untrained MTP, taken from
  MTP_templates/ in the code repository and edited
  according to MTP_templates/readme.txt
- fill_with_vacuum.py: python script to add vacuum to the 
selected spherical neighborhoods along each direction
- pre_train.sh: shell script for the initial potential fitting 
- in.my: the input file for `LAMMPS` which allows us running molecular dynamics 
at 670 K using Moment Tensor Potential (MTP) as an interatomic potential
- md_al_mtp.sh: shell script including all the steps (running MD with `LAMMPS`, 
selection of configurations, updating the training set, etc.) for MTP active learning 
- auto.sh: shell script including all the steps (pre-training a potential, creating a data 
file for `LAMMPS`, start of active training) for “one-by-one” atom depositing. 
- coord.py: python script to generate the coordinates of the new atom
- Cu_1620.data: `LAMMPS` data file, initial substrate
- run_lammps.sh: shell script for running MD with `LAMMPS`
- select.sh: shell script for selection of neighborhoods to be calculated
with VASP and added to the training set

To start the tutorial you need:
1. Add the POTCAR file to the /VASP folder
2. Add lmp and mlp binary files to the current folder.
3. Run the script auto.sh using nohup: nohup ./auto.sh & 

